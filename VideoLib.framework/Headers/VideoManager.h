//
//  VideoManager.h
//  Prosense360
//
//  Created by Artem Kulagin on 16.01.17.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VideoManager : NSObject

+ (void)playVideoAppID:(NSString *)appID key:(NSString *)key url:(NSString *)url navigationController:(UINavigationController *)navigationController  statusBarStyle:(UIStatusBarStyle)statusBarStyle;

+ (void)playVideoAppID:(NSString *)appID key:(NSString *)key url:(NSString *)url navigationController:(UINavigationController *)navigationController;

+ (void)exit;

@end
