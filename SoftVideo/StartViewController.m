//
//  UIVStartViewController.m
//  Prosense360
//
//  Created by Artem Kulagin on 16.01.17.
//
//

#import "StartViewController.h"
#import <VideoLib/VideoManager.h>


@implementation StartViewController

- (IBAction)startAction:(id)sender {
    [VideoManager playVideoAppID:@"com.prosense.sampleproject"
                             key:@"qwretyqwerty"
                             url:@"http://cdn.prosense.tv/streams/test/multistream/shlemenko.m3u8"
            navigationController:self.navigationController
                  statusBarStyle:UIStatusBarStyleLightContent
     ];
    
}

@end
